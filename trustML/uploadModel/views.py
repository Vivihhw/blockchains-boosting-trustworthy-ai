from .forms import ModelForm
from django.shortcuts import render,redirect
from .models import ML_model
from uploadData.models import evaluation_result, Data
from uploadData.forms import DataForm
from django.core.files import File
from django.urls import reverse
import numpy as np
from .evaluation.evaluate_model import get_confusion_matrix,get_model_and_data,get_precision_recall_accuracy
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import os


# Create your views here.


def get_model(request):
    # if it's post then process form data
    if request.method == 'POST':
        form = ModelForm(request.POST, request.FILES)
        if form.is_valid():
            # get the creator_id
            # TODO consider transaction maybe after
            instance = ML_model(creator_id = request.user.pk,model_name = request.FILES['model_name'],\
                                model_file = request.FILES('model_file'),\
                                specification = request.FILES('specification'),\
                                weight = request.FILESI('weight'))
            instance.save()
            # if success, redirect to upload origin data
            # return a http response

def home(request):
    # get the model that user uploaded
    if User.is_authenticated:
        model_sets = ML_model.objects.filter(creator = request.user)
        context = {"model_sets":model_sets}
        return render(request,"uploadModel/home.html",context)
    else:
        # TODO redirect to login page
        redirect("http://localhost:8000/")

def model_detail(request, model_id):
    context = {}
    try:
        result_md = evaluation_result.objects.get(model  = model_id, result_type = "MD")
        result_file = result_md.result
        cf = np.loadtxt(result_file, delimiter=",", dtype=int)
        precision,recall,accuracy = get_precision_recall_accuracy(cf)
        context.update({"precision":precision})
        context.update({"recall":recall})
        context.update({"accuracy":accuracy})
        context.update({"f1_score":2*precision*recall/(precision+recall)})
    except ObjectDoesNotExist:
        result_md = None
    try:
        result_dt = evaluation_result.objects.filter(model = model_id, result_type = "DT")
    except ObjectDoesNotExist:
        result_dt = None
    model = ML_model.objects.get(pk = model_id)
    context.update({"model":model})
    if result_md is not None:
        context.update({"result_md":result_md})
    else:
        context.update({"result_md":None})
    if result_dt is not None:
        context.update({"result_dt":result_dt})
    else:
        context.update({"result_dt":None})
    return render(request,'uploadModel/model_detail.html', context)

def uploading(request):
    # import logging
    # logging.basicConfig(filename='mylog.log', level=logging.DEBUG)
    if request.method == 'POST':
        my_model_form = ModelForm(request.POST,request.FILES)
        # logging.debug(my_model_form)
        if my_model_form.is_valid():
            model_doc = my_model_form.save()
            return redirect(reverse('upload_model:model_detail', args = (model_doc.pk,)) )
        # else:
        #     return HttpResponse("Hello World")

def upload_data(request):
    # import logging
    # logging.basicConfig(filename='mylog.log', level=logging.DEBUG)
    if request.method=="POST":
        form = DataForm(request.POST,request.FILES)
        # logging.debug(form)
        # logging.debug(form.is_valid)
        if form.is_valid():
            form_object = form.save()
            data_file = request.FILES["data_file"]
            model_file = ML_model.objects.get(pk = form_object.relative_model.pk).model_file
            model, data = get_model_and_data(model_file,data_file)
            confusion_matrix = get_confusion_matrix(model,data)
            if not request.user.get_is_staff():
                original_data = Data.objects.get(data_type = 'OR', relative_model = form_object.relative_model)

            file = open("myfile.txt","w+")
            django_file = File(file)
            for row in confusion_matrix:
                django_file.write(",".join(str(number) for number in row) +"\n")

            if request.user.get_is_staff():
                type = "MD"
            else:
                type = "DT"
            result = evaluation_result(data = form_object,model = form_object.relative_model, result_type = type , result = django_file,document_count = np.sum(confusion_matrix))
            result.save()
            django_file.close()
            file.close()
            os.remove("myfile.txt")
            return redirect(reverse("upload_model:model_detail",args = (form_object.relative_model.pk,)))