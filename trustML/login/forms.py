from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import User

class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('email', 'wallet_address')

class EditProfileForm(UserChangeForm):

    class Meta:
        model = User
        fields = ('description','wallet_address', 'password')
