# from django.db import models
# from django.core.validators import MinLengthValidator
# # Create your models here.
#
# class User(models.Model):
#     email_address = models.EmailField(unique=True)
#     user_name  = models.CharField(max_length=20)
#     user_password = models.CharField(max_length=20, validators=[MinLengthValidator(4)])
#     user_photo = models.ImageField(upload_to='user_photo/')
#     TESTER = 'TR'
#     ADMIN = 'AD'
#     USER_TYPE_CHOICE = (
#         (TESTER,'Tester'),
#         (ADMIN,'Administrator'))
#     user_type = models.CharField(max_length=2, choices=USER_TYPE_CHOICE,default=TESTER)
#

from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.db import models
import os

class MyUserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, null=True)
    wallet_address = models.CharField(max_length=40, default='NO ADDRESS ADDED')
    image = models.ImageField(
        upload_to='user_photo/',
        blank=True,
    )
    cover = models.ImageField(
        upload_to='user_photo/',
        blank=True,
    )
    description = models.CharField(
        max_length=1000,
        default='Add a short description of yourself.'
    )
    is_staff = models.BooleanField(
        'staff status',
        default=False,
        help_text='Is the user allowed to have access to the admin',
    )
    is_active = models.BooleanField(
        'active',
        default=True,
        help_text= 'Is the user account currently active',
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['wallet_address']
    objects = MyUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        short_name = ''
        for c in self.email:
            if c == '@':
                break
            short_name += c
        return short_name

    def get_is_staff(self):
        return self.is_staff
