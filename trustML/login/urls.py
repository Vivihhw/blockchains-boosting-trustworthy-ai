from django.urls import path
from django.contrib.auth import views as auth_views

from . import views
app_name = 'login'

urlpatterns = [
    path('', auth_views.LoginView.as_view(), name='login'),
    path('user_profile/', views.editprofile, name='profile'),
    path('signup/', views.signup, name='signup'),
    path('profile/', views.editprofile, name='profile'),
]
