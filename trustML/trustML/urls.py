"""trustML URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path
from . import views
from django.contrib import admin

urlpatterns = [
    path('', include('login.urls')),
    path('', include('django.contrib.auth.urls')),
    path('test/', views.test, name='test'),
    path('user_home/', views.user_home, name='user_home'),
    path('user_profile/', views.user_profile, name='user_profile'),
    path('admin_home/', views.admin_home, name='admin_home'),
    path('admin_profile/', views.admin_profile, name='admin_profile'),
    path('register/', views.register, name='register'),
    path('model/', include('uploadData.urls')),
    path('result/', views.result, name='result'),
    path('data/', views.data, name='data'),
    path('run/', views.run, name='data'),
    path('clear/', views.clear, name='data'),
    path('admin/',admin.site.urls),
    path('upload/', include('uploadData.urls')),

    path('upload_model/',include('uploadModel.urls'))


]
