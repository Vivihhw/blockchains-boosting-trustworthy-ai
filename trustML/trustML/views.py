from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.conf import settings
import os
import sys
import shutil
sys.path.append(os.getcwd()+'/models/dog_classifier/')
import dog_classifier


def index(request):
    return render(request, 'index.html')

def test(request):
    return render(request, 'test.html')

@login_required(login_url='/')
def user_home(request):
    return render(request, 'user_home.html')

@login_required(login_url='/')
def user_profile(request):
    return render(request, 'user_profile.html')

@login_required(login_url='/')
def admin_home(request):
    return render(request, 'admin_home.html')

@login_required(login_url='/')
def admin_profile(request):
    return render(request, 'admin_profile.html')

@login_required(login_url='/')
def model(request):
    path=settings.BASE_DIR+'/media/data/'  # insert the path to your directory
    img_list = [f for f in os.listdir(path)  if f.endswith('.jpeg') or f.endswith('.jpg') or f.endswith('.png')]
    return render(request,'model.html', {'images': img_list})

def register(request):
    return render(request, 'register.html')

@login_required(login_url='/')
def result(request):
    path=settings.BASE_DIR+'/models/dog_classifier/'  # insert the path to your directory
    img_list = [f for f in os.listdir(path)  if f.endswith('results.png')]
    return render(request,'result.html', {'images': img_list})

def data(request):
    path=settings.BASE_DIR+'/media/data/'  # insert the path to your directory
    img_list = [f for f in os.listdir(path)  if f.endswith('.jpeg') or f.endswith('.jpg') or f.endswith('.png')]
    return render(request,'data.html', {'images': img_list})
    # return render(request, 'data.html')

def run(request):
    print(os.getcwd())
    dog_classifier.classify()
    return redirect('/result')

def clear(request):
    path = settings.BASE_DIR+'/media/data/'
    for the_file in os.listdir(path):
        file_path = os.path.join(path, the_file)
        try:
            if (os.path.isfile(file_path) and (file_path.endswith('.jpeg') or file_path.endswith('.jpg') or file_path.endswith('.png'))):
                os.unlink(file_path)
            #elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    return redirect('/model')
