import math

class EvaluatedDataset:
    def __init__(self, predicted, actual, similarity, TP, TN, FP, FN):
        self.predicted = predicted
        self.actual = actual
        self.similarity = similarity
        self.TP = TP
        self.TN = TN
        self.FP = FP
        self.FN = FN

    def getLength(self):
        return len(self.predicted)

    def getAccuracy(self):
        if (len(self.predicted) == 0):
            return 0
        return (self.TP+self.TN)/len(self.predicted)

    def getPrecision(self):
        if (self.TP == 0):
            return 0
        return self.TP/(self.TP+self.FP)

    def getRecall(self):
        if (self.TP == 0):
            return 0
        return self.TP/(self.TP+self.FN)

    def getNPrecision(self):
        if (self.TN == 0):
            return 0
        return self.TN/(self.TN+self.FN)

    def getNRecall(self):
        if (self.TN == 0):
            return 0
        return self.TN/(self.TN+self.FP)

    def getF1Score(self):
        if (self.getPrecision(self)+self.getRecall(self) == 0):
            return 0
        return (2*self.getPrecision(self)*self.getRecall(self))/(self.getPrecision(self)+self.getRecall(self))

    # Single score that evalutes the dataset, given a weighting, and using the similarity and length
    def getScoreWithWeighting(self, weighting):
        var = 0.3
        mean = 0.7
        a = 0.001
        multiplier = 100

        # Similarity is placed on a Gaussian distribution with the peak at 'mean' above.
        # This is so that datasets with moderate similarity rank highest.
        #       Such datasets have different enough data to be useful for testing
        #       Yet similar enough data that they are maximally applicable to the model
        similarityToValue = (1/(math.sqrt(2*math.pi*var)))*math.exp(-((self.similarity-mean)**2)/(2*var))
        weightedAccuracy = 0.3 + self.getNRecall(self)*(1-weighting) + (self.getRecall(self)*weighting)
        weightedPrecision = 0.3 + self.getNPrecision(self)*(1-weighting) + (self.getPrecision(self)*weighting)
        lengthToValue = 1.3 - 1/(1+a*self.getLength(self))
        #This is a bad score function TODO Fix it!
        score = similarityToValue*lengthToValue*multiplier*weightedAccuracy*weightedPrecision
        return score
