#Generates a random binary csv file to be used to test
#modelEvaluation.py

import csv
import random

pred = []
actu = []

def generateDummyList(length):
    for i in range(0,length):
        pred.append(random.randint(0,1))
        actu.append(random.randint(0,1))

def saveDummyFile(fileName):
    with open(fileName, 'w') as myFile:
        wr = csv.writer(myFile)
        for i in range(0, len(pred)):
            wr.writerow(str(pred[i]) + str(actu[i]))

generateDummyList(1000)
saveDummyFile("dummy.csv")
