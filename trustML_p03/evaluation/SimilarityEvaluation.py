# This script should accept two numerical matrices, representing the
# training set and the testing set, and will output a similarity metric

# The similarity is the weighted sum of the min distance from each testing point
# to any training point.

# Note, this runs in n*n'*p time, where n is the number of training set points,
# n' is the number of testing set points, and p is the number of attributes.
# Hence, the time taken here will be massive for large sets, and be sets with large
# number of attributes (like pictures).

import math

def FindClosestPointDistance(trainingSet, testPoint, attrWeighting):
    minDist = 1000000;
    for i in range(len(trainingSet)):
        thisDist = 0
        for j in range(len(testPoint)):
            thisDist += attrWeighting[j]*((trainingSet[i][j] - testPoint[j])**2)
        thisDist = math.sqrt(thisDist)
        if (thisDist < minDist):
            minDist = thisDist
    return minDist

def GetSimilarity(trainingSet, testingSet, attrWeighting):
    # Check that they have the same number of attributes.
    if (len(trainingSet[0]) != len(testingSet[0])):
        print("Sets have different number of columns")
        return
    elif (len(trainingSet[0]) != len(attrWeighting)):
        print("Attribute weighting incorrect length")
        return

    similarity = 0

    # Evaluate the similarity using weighted KL-divergence
    for i in range(len(testingSet)):
        similarity += FindClosestPointDistance(trainingSet, testingSet[i], attrWeighting)

    return similarity
