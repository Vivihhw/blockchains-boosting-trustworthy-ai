import csv
N_COLS = 10

def evaluate_data(file):
    data = list(csv.reader(file))
    for row in data:
        if len(row) != 10:
            return 0
    return len(data)

if __name__ == "__main__":
    val = evaluate_data("test.csv")