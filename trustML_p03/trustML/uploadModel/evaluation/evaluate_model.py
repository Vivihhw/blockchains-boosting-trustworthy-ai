from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
import numpy as np
import math

def get_model_and_data(model_file, data_file):
    model = joblib.load(model_file)
    # data = np.loadtxt(data_file,delimiter=",",dtype=float)
    # print(data[0])
    data = np.loadtxt(data_file,delimiter=",", dtype=float)
    return model, data

# get a confusion matrix which will be used later
def get_confusion_matrix(model,data):
    import logging
    logging.basicConfig(filename='mylog.log', level=logging.DEBUG)
    """
    :param model:
    :param data:
    :return: a confusion matrix with Y-axis as true label X-axis as predict label
    """
    my_model = model
    # logging.debug("result: ")
    # logging.debug(data[:, :data.shape[1] - 1])
    result = my_model.predict(data[:,:data.shape[1]-1])
    cm = confusion_matrix(data[:,data.shape[1]-1],result)
    return cm

def get_precision_recall_accuracy(confusion_matrix):
    """
    :param confusion_matrix: get from sklearn package
    :return: precisions, recalls -> arrays, accuracy -> a float number
    """
    if confusion_matrix.shape[0] == 2:
        recall = np.zeros(1)
        precision = np.zeros(1)
        tp = confusion_matrix[0,0]
        fp = confusion_matrix[1,0]
        fn = confusion_matrix[0,1]
        tn = confusion_matrix[0,0]

        precision[0] = 0
        if tp + fp != 0:
            precision[0] = float(tp) / (tp + fp)

        recall[0] = 0
        if tp + fn != 0:
            recall[0] = float(tp) / (tp + fn)

        accuracy = 0
        if tp + fp + fn + tn != 0:
            accuracy = float(tp + tn)/(tp + fp + fn + tn)
    else:
        recall = np.zeros(confusion_matrix.shape[0])
        precision = np.zeros(confusion_matrix.shape[0])
        correctly_predicted = 0
        for row_idx in range(confusion_matrix.shape[0]):
            precision[row_idx] = float(confusion_matrix[row_idx][row_idx])/ np.sum(confusion_matrix[:,row_idx])
            recall[row_idx] = float(confusion_matrix[row_idx][row_idx])/np.sum(confusion_matrix[row_idx,:])
            correctly_predicted += confusion_matrix[row_idx][row_idx]
        accuracy = float(correctly_predicted)/float(sum(confusion_matrix))
    return precision, recall, accuracy

def FindClosestPointDistance(trainingSet, testPoint, attrWeighting):
    minDist = 1000000000000;

    #Step through all the trainingSet data points
    for i in range(len(trainingSet)):
        thisDist = 0
        #For each attribute, add the weighted distance from the testPoint
        for j in range(len(testPoint)):
            thisDist += attrWeighting[j]*((trainingSet[i][j] - testPoint[j])**2)
        thisDist = math.sqrt(thisDist)
        #Find the training point with the minimum distance
        if (thisDist < minDist):
            minDist = thisDist
    return minDist

def get_similarity(trainingSet, testingSet, attrWeighting):
    # Check that they have the same number of attributes.
    if (len(trainingSet[0]) != len(testingSet[0])):
        print("Sets have different number of columns")
        return
    elif (len(trainingSet[0]) != len(attrWeighting)):
        print("Attribute weighting incorrect length")
        return

    #Normalise the data
    maxTrain = max(max(x) for x in trainingSet)
    maxTest = max(max(x) for x in testingSet)
    maxVal = max(maxTest, maxTrain)
    trainingSet *= 1/maxVal
    testingSet *= 1/maxVal

    #attrWeighting should sum to 1
    attrWeighting = [x * (1/sum(attrWeighting)) for x in attrWeighting]

    totalDistance = 0

    # Evaluate the similarity using weighted KL-divergence
    for i in range(len(testingSet)):
        totalDistance += FindClosestPointDistance(trainingSet, testingSet[i], attrWeighting)

    averageDistance = totalDistance/len(testingSet)
    similarity = 1-min(1, averageDistance)

    return similarity

# Weighting here is the importance of getting positive values correct, rather than negative values correct
def get_score(confusion_matrix, weighting, similarity):
    var = 0.3
    mean = 0.7
    a = 0.001
    multiplier = 100
    precision, recall, accuracy = get_precision_recall_accuracy(confusion_matrix)
    precisionN = float(confusion_matrix[1,1])/(confusion_matrix[1,1]+confusion_matrix[0,1])
    recallN = float(confusion_matrix[1,1])/(confusion_matrix[1,1] + confusion_matrix[1,0])
        # Similarity is placed on a Gaussian distribution with the peak at 'mean' above.
        # This is so that datasets with moderate similarity rank highest.
        #       Such datasets have different enough data to be useful for testing
        #       Yet similar enough data that they are maximally applicable to the model
    similarityToValue = (1/(math.sqrt(2*math.pi*var)))*math.exp(-((similarity-mean)**2)/(2*var))
    weightedAccuracy = 0.3 + recallN*(1-weighting) + recall[0]*weighting
    weightedPrecision = 0.3 + precisionN*(1-weighting) + precision[0]*weighting
    lengthToValue = 1.3 - 1/(1+a*np.sum(confusion_matrix))

    score = similarityToValue*lengthToValue*multiplier*weightedAccuracy*weightedPrecision
    return score
#get_score(get_confusion_matrix(model, [[0, 1, 0, 1], [1, 1, 1, 0]]),0.5,0.5)
