import sys
import os
import math
import heapq
import numpy as np
from heapq import heappush, heappop


# A function used to indicate whether a string could be converted to double


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


# A function used to calculate the euclidean_distance between two instances


def euclidean_distance(train, test):
    # If the number of attributes in training instance(including class) is not equal to
    # the number of attributes in testing instance + 1, there is an error

    if (len(train) != len(test) + 1):
        print("Invalid parameters for Euclidean distance")
        sys.exit()
    else:
        # Calculate the euclidean_distance between the two instances
        sum = 0.0
        for i in range(len(test)):
            if (isfloat(train[i]) and isfloat(test[i])):
                sum += math.pow(float(train[i]) - float(test[i]), 2)
        return math.sqrt(sum)


def nb_classifier(training_file, testing_file):
    # Read the training file and process the data into 2D list
    training_file_object = open(training_file, "r")
    training_lines = training_file_object.readlines();
    # Initialise the training list
    training_list = []
    for i in range((len(training_lines[0].rstrip().split(',')) - 1) * 2):
        training_list.append([])
    # Create a variable to count the number of no
    number_no = 0
    # Process the training_lines data and put it into a list that contains 2 * NUM_ATTRIBUTE list
    # The first NUM_ATTRIBUTE lists contain the list of 1st, 2nd, ... NUM_ATTRIBUTEth attrbitute of data instances which are classified as yes
    # The second NUM_ATTRIBUTE lists contain the list of 1st, 2nd, ... NUM_ATTRIBUTEth attrbitute of data instances which are classified as no
    for line in training_lines:
        # Transform the string into a list by delimiting the commas
        data_instance = line.rstrip().split(',')
        # Set up a variable to help to decide which list to append when inserting the attribute of each data instance
        temp = 0
        if (data_instance[-1] == 'no'):
            temp = 1
            number_no += 1
        # Insert the attributes into each list of the data_instance
        for i in range(len(data_instance) - 1):
            if (isfloat(data_instance[i])):
                training_list[temp * (len(data_instance) - 1) + i].append(float(data_instance[i]))
            else:
                print("Invalid attribute value")
                sys.exit()
    # Read the testing file
    testing_file_object = open(testing_file, "r")
    testing_lines = testing_file_object.readlines()
    for line in testing_lines:
        test = line.rstrip().split(',')
        probability_yes = (len(training_lines) - number_no) / len(training_lines)
        probability_no = number_no / len(training_lines)
        # Calculate the probability of class yes and class no
        for i in range(len(test)):
            # Calculate the probability of the current attribute under the class yes
            if (isfloat(test[i])):
                current_attribute_prob_yes = math.exp(-math.pow(float(test[i]) - np.mean(training_list[i]), 2) / (
                            2 * math.pow(np.std(training_list[i], ddof=1), 2))) / (
                                                         np.std(training_list[i]) * math.sqrt(2 * math.pi))
                probability_yes *= current_attribute_prob_yes
                current_attribute_prob_no = math.exp(
                    -math.pow(float(test[i]) - np.mean(training_list[i + len(test)]), 2) / (
                                2 * math.pow(np.std(training_list[i + len(test)], ddof=1), 2))) / (
                                                        np.std(training_list[i + len(test)]) * math.sqrt(2 * math.pi))
                probability_no *= current_attribute_prob_no
            else:
                print("Invalid attribute value")
                sys.exit()
        if (probability_yes >= probability_no):
            print("yes")
        else:
            print("no")


# A function used to classify data instances using the nearest neighbour algorithm
def nn_classifier(training_file, testing_file, k):
    # Read the training file and process the data into 2D list
    training_file_object = open(training_file, "r")
    training_lines = training_file_object.readlines()
    training_list = []
    for line in training_lines:
        training_list.append(line.rstrip().split(','))
    # Read the testing file
    testing_file_object = open(testing_file, "r")
    testing_lines = testing_file_object.readlines()
    for line in testing_lines:
        test = line.rstrip().split(',')
        # Create a priority queue composed of pairs(Euclidean distance, class)
        # class: 0 - no, 1 - yes
        pq = []
        for train in training_list:
            if (train[-1] == 'yes'):
                heappush(pq, (euclidean_distance(train, test), 1))
            elif (train[-1] == 'no'):
                heappush(pq, (euclidean_distance(train, test), 0))
            else:
                print("Invalid class value which doesnt belong to either yes or no")
                sys.exit()
        # Get a list of the smallest n pairs in the priority queue
        n_smallest = heapq.nsmallest(k, pq)
        result = np.mean([sublist[1] for sublist in n_smallest])
        # If the mean is larger than 0.5, that means the majority of the classes are yes
        # Otherwise, it would be no
        # When there is a tie between the number of yes and the number of no, we choose to
        # specify the instance as yes according to the specifications
        if (result >= 0.5):
            print("yes")
        else:
            print("no")


# Reading the input - training_file, testing_file and algorithm
training_file = ""
testing_file = ""
algorithm = ""
if (len(sys.argv) != 4):
    print("You must enter three arguments")
    print("1. Path to the training data file")
    print("2. Path to the testing data file")
    print("3. Choice of the algorihtm - NB for Naive Bayes and kNN for the Nearest Neighbour")
    sys.exit()
else:
    training_file = sys.argv[1]
    testing_file = sys.argv[2]
    algorithm = sys.argv[3]

# Testing if the files exist
if not os.path.isfile(training_file):
    print("Training file does not exist. Please try again.")
    sys.exit()
if not os.path.isfile(testing_file):
    print("Testing file does not exist. Please try again.")
    sys.exit()



# Testing if the input of the algorithm is valid
# If yes, call the corresponding classifier
# If not, quit the program
if (algorithm == 'NB'):
    nb_classifier(training_file, testing_file)
elif (len(algorithm) >= 3 and algorithm[-2:] == 'NN'):
    number = algorithm[:-2]
    if (number.isdigit()):
        k = int(number)
        if (k >= 1):
            nn_classifier(training_file, testing_file, k)
    else:
        print("Invalid choice of k - must be a positive integer. Please try again")
else:
    print("Invalid choice of algorithm - NB for Naive Bayes and kNN for the Nearest Neighbour. Please try again")
