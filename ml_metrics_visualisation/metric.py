"""
machine learning metrics visualisations
@author Marshall Huo
@Date 30/08/2018

Some definitions:
taking recognise cancer as an example, True = the person has a cancer.
False = the person doesnt' have a cancer
TP: the number of people having cancer tested as having cancer
TN: the number of people not having cancer tested as not having cancer
FP: the number of people not having cancer tested as having cancer
FN: the number of people having cancer tested as not having cancer
Accuracy: (TP+TN)/(TP+TN+FP+FN)
precision: TP/(TP+FP)
recall: TP/(TP+FN)
Specificity: FP/(FP+TN)
F1 score: 2*precision*recall/(precision + recall)
"""

from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt
import itertools


# visualisation for classifications

# output a confusion matrix
def plt_confusion_matrix(confusion_matrix,classes,title= 'Confusion Matrix', cmap = plt.cm.Blues, normalised = False):
    """
    :param confusion_matrix: numpy arrary of confusion matrix, can be get by
            '''confusion_matrix(y_test,y_predict)'''
    :param classes: the names of y labels
    :param title: can be customised
    :param cmap: the color of table, can be visualised
    :param normalised: whether to normalise the confusion matrix
    :return: a confusion matrix plot
    """
    if normalised:
        # normalise each row
        confusion_matrix = confusion_matrix.astype(float)/confusion_matrix.sum(axis=1)[:,np.newaxis]
    plt.imshow(confusion_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalised else 'd'
    thresh = confusion_matrix.max() / 2.
    for i, j in itertools.product(range(confusion_matrix.shape[0]), range(confusion_matrix.shape[1])):
        plt.text(j, i, format(confusion_matrix[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if confusion_matrix[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


# to output a score for the performance of a model relative to a data set
def out_score(confusion_matrix,alphas = "not specified"):
    """
    :param confusion_matrix: the confusion matrix as input
    :param alphas: the weight of precision/recall commonly used one: alpha = 1, 0.5(precision weight more than recall), 2(recall weight more than precision)
    :return: a score for the model according to confusion_matrix
    """
    if alphas == "not specified":
        alphas = np.ones(confusion_matrix.shape[0])
    # if alpha = default -> then it's h1 score
    if confusion_matrix.shape[0] == 2:
        # if it's 2 then it's binary classification -> normal confusion matrix
        precision = float(confusion_matrix[0][0])/float(confusion_matrix.sum(axis = 1)[0])
        recall = float(confusion_matrix[0][0])/float(confusion_matrix.sum(axis = 0)[0])
        return (1+np.square(alphas[0]))*precision*recall/(np.square(alphas[0])*precision + recall)
    else:
        # if it's multiclass, the metrics are defined by the person who upload the model
        precisions = np.zeros(confusion_matrix.shape[0])
        recalls = np.zeros(confusion_matrix.shape[0])
        for row_idx in range(confusion_matrix.shape[0]):
            precisions[row_idx] = float(confusion_matrix[row_idx][row_idx])/float(confusion_matrix.sum(axis = 1)[row_idx])
        print(precisions)
        for col_idx in range(confusion_matrix.shape[1]):
            recalls[col_idx] = float(confusion_matrix[col_idx][col_idx])/float(confusion_matrix.sum(axis = 0)[col_idx])
        print(recalls)
        h1_scores = np.zeros(confusion_matrix.shape[0])
        for idx in range(h1_scores.shape[0]):
            h1_scores[idx] = (1+np.square(alphas[idx]))*precisions[idx]*recalls[idx]/(np.square(alphas[idx])*precisions[idx]+recalls[idx])
        return h1_scores.sum()/h1_scores.shape[0]