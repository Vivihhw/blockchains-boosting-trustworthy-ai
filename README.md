# Blockchains Boosting Trustworthy AI

Django Application that uses Ethereum Blockchain to submit data against a ML model,  provide feedback on various aspects of the data and reward top data submissions.

## NOTE: the final project is in the folder labelled trustML_p03. 

## Install Dependencies

### Installing the Solidity Compiler

**For OSX**
Install a these homebrew packages:
```
brew install pkg-config libffi autoconf automake libtool openssl
```
Install the solidity compiler (solc):
```
brew update
brew upgrade
brew tap ethereum/ethereum
brew install solidity
brew link solidity
```
**For Linux**
```
sudo add-apt-repository ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install solc libssl-dev
```

### Firstly you need to install Geth
  [Geth](https://github.com/ethereum/go-ethereum/wiki/geth) is a multipurpose command line tool that runs a full Ethereum node implemented in Go. It offers three interfaces: the command line subcommands and options, a Json-rpc server and an interactive console.

  To install Geth type in the following command in terminal.
  You can also use a one-line script install Geth. Open a command line or terminal tool and paste the command below:
  ```
  bash <(curl -L https://install-geth.ethereum.org)
  ```
  This will detect your OS and will attempt to install the ethereum CLI.
  Also the OS specific installations

  **For OSX**
  Install a these homebrew packages:
  ```
  brew tap ethereum/ethereum
  brew install ethereum
  brew install ethereum --devel
  ```
  **For Linux**
  ```
  sudo apt-get install software-properties-common
  sudo add-apt-repository -y ppa:ethereum/ethereum
  sudo apt-get update
  sudo apt-get install ethereum
  ```


### Initialize your Virtual Environment

Install [virtualenv](https://virtualenv.pypa.io/en/stable/) if you don't have it yet. (Comes installed with [Python3.6](https://www.python.org/downloads/))
Note: Python3.6 or lower is required, One of the packages tensorflow doesn't currently work with Python3.7

Setup a virtual environment with Python 3:
```
cd trustML;
python3.6 -m venv venv;
source venv/bin/activate;
```
### Install python dependencies
  Type the following command in the terminal to install the python libraries:
  ```
  pip3 install -r requirements.txt
  ```

## Run Application

### Run the Blockchain
You will need to run the Geth to run a blockchain locally for the application to connect to.

The blockchain.sh script connects sets up the geth blockchain with its required commands
Type in the following command in terminal:
 ```
 bash blockchain.sh
 ```
### Deploy Contract and Run Django
Open another terminal and make sure you are in the directory containg the main.py file.


 Note: If the contract has already been deployed, you won't need to deploy it again just move to the next step.
 Run the bash script to deploy the smart contract :
 ```
 bash deploy_contract.sh
 ```
 Run the bash script to run Django Application :
 ```
 bash run.sh
 ```
 Open the link running the application on http://127.0.0.1:8000/

## Run Tests
Run the blockchain
```
bash blockchain.sh
```
In a different terminal change into the directory to "tests". Type in the following command:
```
bash run_test.sh
```
This will run all the blockchain tests and give feedback on which ones where sucessfull and any which failed. 
Now, run the tests for the website by navigating back to the "trustML" directoy and typing:
```
python manage.py test
```

### Evaluation
  multiDatasetEvaluation accepts a dataset object of the type EvaluatedDataset, with at least
  the predicted and actual arrays defined. The predicted array is the predicted discrete
  classification of each dataset entry by the model, and the actual array is the actual
  labels for each of those dataset entries.

  FillDatasetObject fills in the confusion matrix for that EvaluatedDataset object, given
  the training set and attribute weighting for the similarity.

  The EvaluatedDataset object functions can then be called to return useful evaluation
  metrics, including length, accuracy, precision, recall, negative precision, negative recall (precision and recall for TN's rather than TP's), F1 Score, and, importantly, the simple
  score.

  The simple score is a single measure of a dataset's quality, which accepts a weighting
  of the importance of getting True Positives, against the importance of getting True Negatives,
  as defined by the user. It also takes into account the datasets length and it's similarity to
  the training set, and returns a single score.

  The similarity is calculated in SimilarityEvaluation, given the attribute weighting, the
  testing set, and the training set. It's a very slow calculation [O(n*nt*p) time], which
  calculates the straight line distance from each point in the test set to the nearest point
  in the training set using weighted attributes. The reason the attributes are weighted is so
  that the model creator can decide which elements of similarity matter more. Suppose you were
  comparing houses, and the attributes were the number of bedrooms, bathrooms, floor area, etc.
  It might be the case that the number of bedrooms is an important attribute in deciding the similarity of houses, so that can be manually weighted higher than the others if so decided.
