pragma solidity ^0.4.19;
// Modified from Danku_demo.sol (https://github.com/algorithmiaio/danku/tree/master/contracts)
// Credit to Dan and Ku

contract Uknad {
  function Uknad() public {
    // Empty
  }

  // Submissions are test datasets
  struct Submission {
      address payment_address;

      // Number of entries in test dataset
      uint n_rows;
      // Number of features + 1
      uint n_cols;

      // Dynamic list of length n_cols arrays (https://ethereum.stackexchange.com/questions/11870/create-a-two-dimensional-array-in-solidity)
      int[n_cols][] test_data
  }

  struct NeuralNet {
    // Define the number of neurons each layer has.
    uint num_neurons_input_layer;
    uint num_neurons_output_layer;
    // There can be multiple hidden layers.
    uint[] num_neurons_hidden_layer;
    // Weights indexes are the following:
    // weights[l_i x l_n_i x pl_n_i]
    // Also number of layers in weights is layers.length-1
    int[] weights;
    int[] biases;
  }

  address public model_creator;
  // Currently, only support one submission
  int256 public model_accuracy = 0;
  bool public test_data_submitted = false;

  // Deadline for submitting test datasets in terms of block size
  // All test datasets will then automatically be evaluate_model
  // Submitters will be paid
  // The model will be tested
  uint public submission_stage_block_size = 241920; // 6 weeks timeframe

  NeuralNet nn_model;
  uint n_features;

  Submission submission;
  bool public contract_terminated = false;
  // Solidity cannot deal with floats, so we use a multiplier
  // Integer precision for calculating float values for weights and biases
  // Example: 9900 represents 99.00
  int constant int_precision = 10000;

  uint8 public stage = 0;
  uint public stage_0_block_height;

  // Takes in array of hashed data points of the entire dataset,
  // submission and evaluation times
  function initiate(NeuralNet nn_model, address model_creator_address, uint n_features) external {
    assert(contract_terminated == false);
    // Ensure this function is called in order
    assert(stage == 0);
    model_creator = model_creator_address;
    stage = 1;
    stage_0_block_height = block.number;

    nn_model = nn_model;
    n_features = n_features;
  }

  function submit_data(
    address payment_address,
    Submission submission) public {
      assert(contract_terminated == false);
      assert(init_level == 1);
      assert(block.number < stage_0_block_height + submission_stage_block_size);
      assert(submission.n_cols == n_features + 1);
      assert(submission.n_rows > 0)
      submission = submission
      test_data_submitted = true;
  }

}
